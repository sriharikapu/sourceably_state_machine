pragma solidity ^0.4.18;

interface StateMachine {
  function performStateChange(bytes32 typeId, bytes32 cycleId, bytes32 nextState) returns (bool);
}
